from flask import Flask, render_template, request
import os

app = Flask(__name__)

@app.route("/")
def hello():
	return "UOCIS docker demo!"

@app.errorhandler(403)
def error_403(name):
	return render_template("403.html"), 403

@app.errorhandler(404)
def error_404(name):
	return render_template("404.html"), 404
	
@app.route("/<path:path>")
def good(path):
	if("~" in path or "//" in path or ".." in path):
		return render_template("403.html"), 403	
	if(os.path.exists('./templates/'+path)):
		return render_template("trivia.html"), 200
	else:
		return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
